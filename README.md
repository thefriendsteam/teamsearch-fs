# README #

Думаем для чего же нужен файловый сервер, мб использовать Express на front-end и все, и этого будет достаточно?

Для чего:

* разделение логики
* доступен только после авторизации
* хранилище файлов находится на более низком уровне доступа
* хранится будут только файлы принадлежащие пользователю: 
     * аватар 
     * вложения
     * CV
     * Code Convention команды
 
Таким образом получается что front-end отдает статику типа:
 
* css 
* html
* js
* img
* icon 

а файловый сервер работает с отдельным хранилищем и доступен только после авторизации  

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact